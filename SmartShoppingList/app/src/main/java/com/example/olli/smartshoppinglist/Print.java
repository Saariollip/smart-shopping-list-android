package com.example.olli.smartshoppinglist;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class Print extends Activity {

    EditText inputText;
    EditText product_amount_text_input;
    EditText product_price_input;
    TextView displayText;
    TextView my_list_text;
    DataBaseHandler dataBaseHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.print);
        inputText = (EditText) findViewById(R.id.inputText);
        product_amount_text_input = (EditText) findViewById(R.id.product_amount_text_input);
        product_price_input = (EditText) findViewById(R.id.product_price_input);
        displayText = (TextView) findViewById(R.id.displayText);



        dataBaseHandler = new DataBaseHandler(this, null, null, 1);
        printDatabaseToDisplay();
        //printDatabaseToList();

        /* Creating dropdown -list for the units of the products */
        /*
        Spinner dropdown = (Spinner)findViewById(R.id.amount_type);
        String[] items = new String[]{"g", "dl", "oz"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
        */
    }

    public void addButtonClicked(View view) {
       Products product = new Products(inputText.getText().toString(), product_amount_text_input.getText().toString(), product_price_input.getText().toString());
       // Products product = new Products(inputText.getText().toString());
        dataBaseHandler.addProduct(product);
        printDatabaseToDisplay();
    }

    public void deleteButtonClicked(View view) {
        String productToDelete = inputText.getText().toString();
        dataBaseHandler.deleteProduct(productToDelete);
        printDatabaseToDisplay();
    }

    public void saveButtonClicked(View v)
    {

        // Starting the Display -activity

        setContentView(R.layout.my_lists);
        /*
        String dbString = dataBaseHandler.productToString();


        TextView view = (TextView) findViewById(R.id.my_list_text);
        // Changing the text


        Intent i = new Intent(this, Display.class);
        view.setText(dbString);

        startActivity(i);

        */
        //TextView view = (TextView) findViewById(R.id.my_list_text);
        // Changing the text
        //view.setText("jee");


    }
    public void nameButtonClicked(View view)
    {
        setContentView(R.layout.my_lists);
        printDatabaseToList();
    }
    public void printDatabaseToDisplay() {
        String dbString = dataBaseHandler.productNameToString();
        displayText.setText(dbString);
        inputText.setText("");
        product_amount_text_input.setText("");
        product_price_input.setText("");
    }
    public void printDatabaseToList()
    {
        String dbString = dataBaseHandler.productToString();


        TextView view = (TextView) findViewById(R.id.my_list_text);
        // Changing the text
        view.setText(dbString);

       // my_list_text.setText(dbString);
        /*
       dataBaseHandler.getAllProducts();
        product_amount_text_input.setText("");
        product_price_input.setText("");
        */

    }

}