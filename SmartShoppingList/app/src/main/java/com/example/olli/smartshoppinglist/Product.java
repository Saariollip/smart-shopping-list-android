package com.example.olli.smartshoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Olli on 14.5.2016.
 */
public class Product extends Activity {



    /*Function for accessing products layout*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.products);
    }

    /**
     * Creating access to the add product Class
     * @param v
     */
    public void onButtonClick(View v) {

        if (v.getId() == R.id.new_product_button) {
            Intent i3 = new Intent(Product.this, Print.class);
            startActivity(i3);

        }
    }
}