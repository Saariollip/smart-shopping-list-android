package com.example.olli.smartshoppinglist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class DataBaseHandler extends SQLiteOpenHelper {


    public static final String TAG = "DataBaseHandler";

    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "products.db";

    /* creating table products */
    public static final String TABLE_PRODUCTS = "products";
    /* columns of the table */
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PRODUCTNAME = "_productname";
    public static final String COLUMN_PRODUCTAMOUNT = "_productamount";
    public static final String COLUMN_PRODUCTPRICE = "_productprice";

    /*Creating table productlists */
    public static final String TABLE_PRODUCTLISTS= "productlists";
    public static final String COLUMN_PRODUCTLISTID = "productlist_id";
    public static final String COLUMN_PRODUCTLISTNAME = "productlist_name";

    private static final String CREATE_TABLE_PRODUCTS = "CREATE TABLE " + TABLE_PRODUCTS + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_PRODUCTNAME + " TEXT NOT NULL, "
            + COLUMN_PRODUCTAMOUNT + " TEXT, "
            + COLUMN_PRODUCTPRICE + " TEXT "
            + ");";

    private static final String CREATE_TABLE_PRODUCTLISTS = "CREATE TABLE " + TABLE_PRODUCTLISTS + "("
            + COLUMN_PRODUCTLISTID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_PRODUCTLISTNAME + " TEXT NOT NULL "
            + ");";

    //Constructor.
    public DataBaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {

        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        //context.deleteDatabase(DATABASE_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_TABLE_PRODUCTS);
        sqLiteDatabase.execSQL(CREATE_TABLE_PRODUCTLISTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG,
                "Upgrading the database from version" + oldVersion + " to "+ newVersion);
        // Clear tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTLISTS);


        //recreates database
        onCreate(db);
    }

    //Add a new row to the database.
    public void addProduct(Products product) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PRODUCTNAME, product.get_productname()); //Where, what. (Not writing to database yet.)

        values.put(COLUMN_PRODUCTAMOUNT, product.get_productamount());
        values.put(COLUMN_PRODUCTPRICE, product.get_productprice());

        sqLiteDatabase.insert(TABLE_PRODUCTS, null, values); //Writing to database now.
        sqLiteDatabase.close();
    }

    public void addProductName(Products product)
    {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PRODUCTNAME, product.get_productname()); //Where, what. (Not writing to database yet.)


        sqLiteDatabase.insert(TABLE_PRODUCTS, null, values); //Writing to database now.
        sqLiteDatabase.close();
    }
    //Delete a product from the database.
    public void deleteProduct(String productName) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String query = "DELETE FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCTNAME + "=\"" + productName + "\"";
        sqLiteDatabase.execSQL(query);
    }

    public void addProductList(Productlists productlist)
    {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PRODUCTLISTNAME, productlist.getProductlist_name()); //Where, what. (Not writing to database yet.)

        sqLiteDatabase.insert(TABLE_PRODUCTLISTS, null, values); //Writing to database now.
        sqLiteDatabase.close();
    }
    /*
    // Getting All products
    public List<Products> getAllProducts() {
        List<Products> productList = new ArrayList<Products>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Products product = new Products();
                product.set_productname(cursor.getString(0));
                product.set_productamount(cursor.getString(1));
                product.set_productprice(cursor.getString(2));
                // Adding contact to list
                productList.add(product);
            } while (cursor.moveToNext());
        }

        // return contact list
        return productList;
    }
    */

    //Print out the product's name
    public String productNameToString() {
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE 1"; //Select every column, select every row.

        Cursor cursor = db.rawQuery(query, null); //Cursor point to a location in results.
        cursor.moveToFirst(); //Move to the first row in results.
        while (!cursor.isAfterLast()) {
            if (cursor.getString(cursor.getColumnIndex("_productname")) != null) {
                dbString += cursor.getString(cursor.getColumnIndex("_productname"));
                dbString += "\n";

            }
            cursor.moveToNext();
        }
        cursor.close();

        db.close();
        return dbString;
    }

    //Print out the product table's content
    public String productToString() {
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE 1"; //Select every column, select every row.


        Cursor cursor = db.rawQuery(query, null); //Cursor point to a location in results.
        cursor.moveToFirst(); //Move to the first row in results.
        while (!cursor.isAfterLast()) {
            if (cursor.getString(cursor.getColumnIndex("_productname")) != null) {
                //dbString += cursor.getString(cursor.getColumnIndex("_productlist"));
                /*
                dbString += "Name:   ";
                dbString += "Amount:    ";
                dbString += "Price:";
                dbString += "\n";
                */
                    dbString += cursor.getString(cursor.getColumnIndex("_productname"));
                    dbString += ", ";
                    dbString += cursor.getString(cursor.getColumnIndex("_productamount"));
                    dbString += ", ";
                    dbString += cursor.getString(cursor.getColumnIndex("_productprice"));
                    dbString += "\n";

            }
            cursor.moveToNext();
        }
        cursor.close();

        db.close();
        return dbString;
    }

    // print's productlist
    /*
    public String productlistToString() {
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE 1"; //Select every column, select every row.


        Cursor cursor = db.rawQuery(query, null); //Cursor point to a location in results.
        cursor.moveToFirst(); //Move to the first row in results.
        while (!cursor.isAfterLast()) {
            if (cursor.getString(cursor.getColumnIndex("_productname")) != null) {
                dbString += cursor.getString(cursor.getColumnIndex("_productlist"));
                dbString += "\n";

            }
            cursor.moveToNext();
        }
        cursor.close();

        db.close();
        return dbString;
    }
    */
}