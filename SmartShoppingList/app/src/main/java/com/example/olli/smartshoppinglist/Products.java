package com.example.olli.smartshoppinglist;

public class Products {

    private int _id;
    private String _productname;
    private String _productamount = "";
    private String _productprice;
    private int _productresource;


    //Constructor 1.

    public Products() {

    }

    //Constructor 2.
    public Products(String _productname) {
        this._productname = _productname;
    }

    //Constructor 3.
    public Products(String _productname, String _productamount, String _productprice) {
        this._productname = _productname;
        this._productamount = _productamount;
        this._productprice = _productprice;
    }

    //Constructor 4.
    public Products(String _productname, String _productamount, int _productresource) {
        super();
        this._productname = _productname;
        this._productamount = _productamount;
        this._productresource = _productresource;
    }

    //Getters and setters.
    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }


    public String get_productname() {
        return _productname;
    }

    public void set_productname(String _productname) {
        this._productname = _productname;
    }

    public String get_productamount() {
        return _productamount;
    }

    public String get_productprice() {
        return _productprice;
    }

    public void set_productprice(String _productprice) {
        this._productprice = _productprice;
    }

    public void set_productamount(String _productamount) {
        this._productamount = _productamount;
    }

    public int get_productresource() {
        return _productresource;
    }

    public void set_productresource(int _productresource) {
        this._productresource = _productresource;
    }
}