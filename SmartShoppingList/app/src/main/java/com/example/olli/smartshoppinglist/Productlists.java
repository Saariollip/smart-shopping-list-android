package com.example.olli.smartshoppinglist;

/**
 * Created by Olli on 17.5.2016.
 */
public class Productlists {

    private int productlist_id;
    private String productlist_name;

    public int getProductlist_id() {
        return productlist_id;
    }

    public void setProductlist_id(int productlist_id) {
        this.productlist_id = productlist_id;
    }

    public String getProductlist_name() {
        return productlist_name;
    }

    public void setProductlist_name(String productlist_name) {
        this.productlist_name = productlist_name;
    }
}
