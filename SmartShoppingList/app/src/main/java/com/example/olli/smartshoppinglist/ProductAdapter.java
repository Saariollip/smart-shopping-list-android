package com.example.olli.smartshoppinglist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olli on 17.5.2016.
 */
public class ProductAdapter extends ArrayAdapter {
    private List list = new ArrayList();

    public ProductAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Products object) {
        list.add(object);
        super.add(object);
    }
    static class ImgHolder
    {
        ImageView IMG;
        TextView NAME;
        TextView AMOUNT;
    }
    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row;
        row = convertView;
        ImgHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row = inflater.inflate(R.layout.row_layout, parent, false);
            holder = new ImgHolder();
            holder.IMG = (ImageView) row.findViewById(R.id.product_img);
            holder.NAME = (TextView) row.findViewById(R.id.product_name);
            holder.AMOUNT = (TextView) row.findViewById(R.id.product_amount);
            row.setTag(holder);

        }
        else
        {
            holder = (ImgHolder) row.getTag();

        }

        Products pr = (Products) getItem(position);
        holder.IMG.setImageResource(pr.get_productresource());
        holder.NAME.setText(pr.get_productname());
        holder.AMOUNT.setText(pr.get_productamount());

        return row;
    }
}
