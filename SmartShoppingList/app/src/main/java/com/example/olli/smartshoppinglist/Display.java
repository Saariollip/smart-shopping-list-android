package com.example.olli.smartshoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Olli on 14.5.2016.
 */
public class Display extends Activity {
    //d
    ListView listview;
    int[] img_resource = {R.drawable.apple, R.drawable.banana, R.drawable.cherry,
    R.drawable.orange,R.drawable.strawberry, R.drawable.mango, R.drawable.tomato};
    String[] name = {"Apple", "Banana", "Cherry", "Orange", "Strawberry", "Mango", "Tomato"};
    String[] amount = {"100 kg","150 kg","300 kg","250 kg","500 kg","300 kg", "220 kg"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_lists);

        TextView view = (TextView) findViewById(R.id.my_list_text);

        //Storing the text view to variable as string
        String lists = view.getText().toString();
;
        //d
        if (lists.equals("No lists found") == true) {

            System.out.println("jee");
        } else {
            listview = (ListView) findViewById(R.id.main_list_view);
            ProductAdapter adapter = new ProductAdapter(getApplicationContext(), R.layout.row_layout);
            listview.setAdapter(adapter);

            int i = 0;
            for (String Name : name) {
                Products prod = new Products(Name, amount[i], img_resource[i]);
                adapter.add(prod);
                i++;
            }

        }
    }

    /**
     * Creating access to the Product -class by clicking the new list -button
     * @param v
     */
    public void onButtonClick(View v) {

        if (v.getId() == R.id.new_list_button) {
            Intent i2 = new Intent(Display.this, Print.class);
            startActivity(i2);

        }
    }
}
